// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB44XxBtX8vDMDCx7-Li_ePZjkGX2UUgqo",
    authDomain: "magic-cards-battleroyale.firebaseapp.com",
    databaseURL: "https://magic-cards-battleroyale-default-rtdb.firebaseio.com",
    projectId: "magic-cards-battleroyale",
    storageBucket: "magic-cards-battleroyale.appspot.com",
    messagingSenderId: "730489825784",
    appId: "1:730489825784:web:4eb548a0c5f31e8a7a8e0d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
