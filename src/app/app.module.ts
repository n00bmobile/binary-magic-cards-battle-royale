import { environment } from 'src/environments/environment';
//MODULES
import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
  //ANIMATIONS
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//COMPONENTS
import { AppComponent } from './app.component';
import { GameComponent } from './components/game/game.component';
import { JoinComponent } from './components/join/join.component';
import { LobbyComponent } from './components/lobby/lobby.component';
import { GmScoreboardComponent } from './components/game/gm-scoreboard/gm-scoreboard.component';
//import { NumDisplayComponent } from './components/game/num-display/num-display.component';

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    JoinComponent,
    LobbyComponent,
    GmScoreboardComponent,
    //NumDisplayComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.threeBounce,
    }),
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
