import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { interval } from 'rxjs';
import { CommunicationService } from 'src/app/services/communication.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {

  //players = [];

  constructor(
    public mp: CommunicationService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.waitForGameStart();
    
    //PLACEHOLDER SONG
    //var audio = new Audio('../../../../assets/sounds/lobby_music.mp3');
    //audio.play();
  }

  startGame(){
    this.mp.startGame();
  }

  waitForGameStart(){
    this.mp.onStarted().then(
      (started) => {
        if (started){ 
          this.route.navigate(['/game']); 
        }
      }
    )
  }

}
