//import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
//import { interval } from 'rxjs';
import { HiddenNumberModel } from 'src/app/models/hidden-number.model';
import { PlayerModel } from 'src/app/models/player.model';
import { AudioService } from 'src/app/services/audio.service';
import { CommunicationService } from 'src/app/services/communication.service';

/*export const fadeAnimation = trigger('fadeAnimation', [
  state('focus', style({
    padding: '15px',
    borderColor: 'red'
  })),
  state('unfocus', style({
    padding: '10px',
    borderColor: 'grey'
  })),
  transition('unfocus => focus', [
    animate('0.15s')
  ]),
  transition('focus => unfocus', [
    animate('0.15s')
  ])
]);*/

@Component({
  selector: 'app-gm-scoreboard',
  templateUrl: './gm-scoreboard.component.html',
  styleUrls: ['./gm-scoreboard.component.scss'],
  //animations: [fadeAnimation]
})
export class GmScoreboardComponent implements OnInit {

  //lastUpdatedPlayersLeftCount!: number;

  constructor(
    public mp: CommunicationService,
    public audioService: AudioService
  ) { }

  ngOnInit(): void {
    //this.loadAudio();
  }

  //playerEliminatedAudio = new Audio('../../../../assets/sounds/ingame/player_eliminated.wav');

  //private loadAudio(){
  //  this.playerEliminatedAudio.load();
  //}

  /*getScoreboard(){
    const scoreboard = this.mp.getPlayerScoreboard();
    const count = this.mp.getPlayersLeft().length;

    if (this.lastUpdatedPlayersLeftCount){
      if (count < this.lastUpdatedPlayersLeftCount){
        this.playerEliminatedAudio.playbackRate = 2-(this.mp.getPlayersLeft().length/scoreboard.length);
        this.playerEliminatedAudio.play();
      }
    }

    this.lastUpdatedPlayersLeftCount = count;
    return scoreboard;
  }*/

  canAttack(target: PlayerModel){
    return (this.mp.LocalPlayer().ammo.length > 0 && this.mp.LocalPlayer().nickname != target.nickname && !this.mp.isPlayerOut(target.nickname));
  }

  /*isHovered(i: number){
    const el = document.getElementById('player_'+i);
    
    if (!el){
      return true;
    }
    else{
      return el.matches(':hover');
    }
  }*/

  attackPlayer(target: PlayerModel){
    if (this.canAttack(target)){
      const hiddenNumber = new HiddenNumberModel();
      hiddenNumber.number = <number>this.mp.localPlayer.ammo.shift();
      hiddenNumber.from_player = this.mp.LocalPlayer().nickname;
      target.numbers.push(hiddenNumber);
      //Play audio
      this.audioService.playAudio('attack');
      //Send data
      this.mp.updatePlayer(this.mp.LocalPlayer());
      this.mp.update(target.nickname, target);
    }
  }

}