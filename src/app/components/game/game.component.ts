import { Component, OnInit } from '@angular/core';
import { CardModel } from 'src/app/models/card.model';
import { CommunicationService } from 'src/app/services/communication.service';
import { interval } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { HiddenNumberModel } from 'src/app/models/hidden-number.model';
import { AudioService } from 'src/app/services/audio.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  /*
  CLICK ON CARD BUG DUE TO EXCESSIVE REFRESHING!!!
  */

  DEBUG_MODE = false;

  CARD_ROWS = 4;
  MAX_NUMBERS = 8;

  MIN_TIME = 1500; //1500;
  STARTING_TIME = 45000; //45000;
  ELAPSED_POWER = 0.815;

  cards: CardModel[] = [];

  constructor(
    public mp: CommunicationService,
    private alert: AlertService,
    private audioService: AudioService
  ) { }

  ngOnInit(): void {
    if (this.DEBUG_MODE){
      this.mp.join('-MjX5Ew4jN_lD7PMhfJl', 'meunick').then(
        () => { this.initGame(); }
      );
    }
    else{
      this.initGame();
      this.attackHandler();
    }
  }

  private initGame(){
    this.populateCards();
    this.addNumbers();
    this.waitToAddNumber();
    //Display
    this.cooldownBarSetup();
    this.displayEffect();

    const src = this.mp.getUpdates().subscribe(
      () => {
        if (this.mp.getWinner()?.nickname == this.mp.LocalPlayer().nickname){
          src.unsubscribe();
          this.alert.notify('success', 10000, 'Você ganhou!');
        }
        else if (this.mp.isPlayerDefeated()){
          src.unsubscribe();
          this.alert.notify('error', 10000, 'Você perdeu!');
        }
      }
    )
  }

  private updateHighlightedCards(){
    for (let i = 0; i < this.cards.length; i++){
      if (this.cards[i].numbers.find(num => (num == this.mp.LocalPlayer().numbers[0]?.number))){
        this.cards[i].highlighted = true;
      }
      else{
        this.cards[i].highlighted = false;
      }
    }
  }

  timeUntilNextNumber: number = 0;
  numberCooldown: number = 0;

  waitToAddNumber(){
    if (this.mp.getWinner() || this.mp.isPlayerDefeated()) { return; }

    const elapsed = this.mp.getMatchTime();
    const delay = Math.max(this.STARTING_TIME - (elapsed**this.ELAPSED_POWER), this.MIN_TIME);
    this.timeUntilNextNumber = (new Date().getTime() + delay);
    this.numberCooldown = delay;
    
    const src = interval(delay).subscribe(
      () => {
        src.unsubscribe();
        this.addNumbers();
        this.waitToAddNumber();
      }
    );
  }

  private addNumbers(amount?: number){
    if (this.mp.getWinner() || this.mp.isPlayerDefeated()) { return; }

    const player = this.mp.LocalPlayer();
    
    for (let i = 0; i < (amount || 1); i ++){
      const hiddenNum = new HiddenNumberModel();
      hiddenNum.number = Math.floor(Math.random()*63)+1;
      player.numbers.push(hiddenNum);
    }
    
    this.mp.updatePlayer(player);
    this.updateHighlightedCards();
    this.updateDisplay();
  }

  private deleteNumber(){
    const player = this.mp.LocalPlayer();
    player.numbers_guessed.push(this.mp.getPlayerNumbers()[0]);
    player.numbers.shift();

    if (player.ammo.length < 3 && player.numbers_guessed.length%5 == 0){
      player.ammo.push(player.numbers_guessed[((player.numbers_guessed.length/5)-1)*5].number);
    }
    
    this.mp.updatePlayer(player);
    this.updateHighlightedCards();
    this.updateDisplay();
  }

  onSelect(origin: CardModel, num: number){
    if (origin.highlighted){
      if (num == this.mp.getPlayerNumbers()[0].number){
        this.audioService.playAudio('rightanswer');
        this.deleteNumber();
      }
      else{
        this.audioService.playAudio('wronganswer');
        this.addNumbers();
      }
    }
  }

  private populateCards(){
    //1st card
    const card1 = new CardModel();
    card1.numbers = [
      32, 33, 34, 35, 36, 37, 38, 39,
      40, 41, 42, 43, 44, 45, 46, 47,
      48, 49, 50, 51, 52, 53, 54, 55,
      56, 57, 58, 59, 60, 61, 62, 63
    ];
    //2nd card
    const card2 = new CardModel();
    card2.numbers = [
      16, 17, 18, 19, 20, 21, 22, 23,
      24, 25, 26, 27, 28, 29, 30, 31,
      48, 49, 50, 51, 52, 53, 54, 55,
      56, 57, 58, 59, 60, 61, 62, 63
    ];
    //3rd card
    const card3 = new CardModel();
    card3.numbers = [
      8, 9, 10, 11, 12, 13, 14, 15,
      24, 25, 26, 27, 28, 29, 30, 31,
      40, 41, 42, 43, 44, 45, 46, 47,
      56, 57, 58, 59, 60, 61, 62, 63
    ];
    //4th card
    const card4 = new CardModel();
    card4.numbers = [
      4, 5, 6, 7, 12, 13, 14, 15,
      20, 21, 22, 23, 28, 29, 30, 31,
      36, 37, 38, 39, 44, 45, 46, 47,
      52, 53, 54, 55, 60, 61, 62, 63
    ];
    //5th card
    const card5 = new CardModel();
    card5.numbers = [
      2, 3, 6, 7, 10, 11, 14, 15,
      18, 19, 22, 23, 26, 27, 30, 31,
      34, 35, 38, 39, 42, 43, 46, 47,
      50, 51, 54, 55, 58, 59, 62, 63
    ];
    //6th card
    const card6 = new CardModel();
    card6.numbers = [
      1, 3, 5, 7, 9, 11, 13, 15,
      17, 19, 21, 23, 25, 27, 29, 31,
      33, 35, 37, 39, 41, 43, 45, 47,
      49, 51, 53, 55, 57, 59, 61, 63
    ];

    this.cards = [card1, card2, card3, card4, card5, card6];
  }

  //HELPERS
  numberToArray(number: number){
    let arr = [];

    for (let i = 0; i < number; i++){
      arr.push(i);
    }

    return arr;
  }

  getSubArray(i: number, array: number[]){
    const div = array.length/this.CARD_ROWS; //=2
    const start = div*i;
    
    if (this.CARD_ROWS > i){
      return array.slice(start, div*(i+1));
    }
    else{
      return array.slice(start);
    }
  }

  /*
      ___ _           _             
     /   (_)___ _ __ | | __ _ _   _ 
    / /\ / / __| '_ \| |/ _` | | | |
   / /_//| \__ \ |_) | | (_| | |_| |
  /___,' |_|___/ .__/|_|\__,_|\__, |
               |_|            |___/
  */

  numericDisplay: string[] = [];

  private cooldownBarSetup(){
    const src = interval(250).subscribe(
      () => {
        if (this.mp.getWinner() || this.mp.isPlayerDefeated()){ 
          src.unsubscribe();
          return;
        }
        
        let bar: any = document.getElementById('time-bar');

        if (bar){
          const timeLeft = (this.timeUntilNextNumber - (new Date()).getTime()); //in seconds
          bar.style.width = ((1-(timeLeft/this.numberCooldown))*100)+'%';
        }
      }
    );
  }

  private updateDisplay(){
    if (this.mp.getWinner() || this.mp.isPlayerDefeated()) { return; }
    
    let display: string[] = [];
    const player = this.mp.LocalPlayer();

    for (let i = 0; i < Math.min(this.MAX_NUMBERS-player.numbers.length, player.numbers_guessed.length); i++){
      display.push(String(player.numbers_guessed[player.numbers_guessed.length-1-i].number));
    }

    for (let i = 0; i < player.numbers.length; i++){
      const attacker = player.numbers[i].from_player;
      
      if (attacker){
        display.push(`${attacker}\n${('0'+(Math.floor(Math.random()*63)+1)).slice(-2)}?`);
      }
      else{
        display.push(`${('0'+(Math.floor(Math.random()*63)+1)).slice(-2)}?`);
      }
    }

    this.numericDisplay = display;
  }

  private displayEffect(){
    const amount = this.mp.getPlayerNumbers().length;
    
    if (amount > 0){
      const src = interval(2000/(amount**1.5)).subscribe(
        () => {
          src.unsubscribe();
          this.updateDisplay();
          this.displayEffect();
        }
      );  
    }
  }

  //AUDIO
  private attackHandler(){
    this.mp.watchLocalPlayerNumbers().subscribe(
      (updated) => {
        if (updated[updated.length-1]?.from_player){
          const rate = 1.5-Math.max(this.numberCooldown/this.STARTING_TIME-0.5, 0);
          
          switch (Math.floor(Math.random()*3)+1){
            case 1:
              this.audioService.attacked01Audio.playbackRate = rate;
              this.audioService.playAudio('attacked01');
              break;
            case 2:
              this.audioService.attacked02Audio.playbackRate = rate;
              this.audioService.playAudio('attacked02');
              break;
            case 3:
              this.audioService.attacked03Audio.playbackRate = rate;
              this.audioService.playAudio('attacked03');
              break;
          }

          this.updateDisplay();
        }
      }
    );
  }

}