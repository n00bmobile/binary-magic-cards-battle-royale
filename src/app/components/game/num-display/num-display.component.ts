import { Component, Input, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { CommunicationService } from 'src/app/services/communication.service';

@Component({
  selector: 'game-num-display',
  templateUrl: './num-display.component.html',
  styleUrls: ['./num-display.component.scss']
})
export class NumDisplayComponent implements OnInit {

  MAX_NUMBERS = 8;

  @Input() numberCooldown: number = 0;
  @Input() timeUntilNextNumber: number = 0;

  numericDisplay: string[] = [];

  constructor(
    public mp: CommunicationService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  private init(){
    this.cooldownBarSetup();
    this.displayEffect();
    this.mp.watchLocalPlayerNumbers().subscribe(
      () => { this.updateDisplay(); }
    )
  }

  private cooldownBarSetup(){
    const src = interval(250).subscribe(
      () => {
        if (this.mp.isPlayerDefeated()){ 
          src.unsubscribe();
          return;
        }
        
        let bar: any = document.getElementById('time-bar');

        if (bar){
          const timeLeft = (this.timeUntilNextNumber - (new Date()).getTime()); //in seconds
          bar.style.width = ((1-(timeLeft/this.numberCooldown))*100)+'%';
        }
      }
    );
  }

  private updateDisplay(){
    let display: string[] = [];
    const player = this.mp.LocalPlayer();

    for (let i = 0; i < Math.min(this.MAX_NUMBERS-player.numbers.length, player.numbers_guessed.length); i++){
      display.push(String(player.numbers_guessed[player.numbers_guessed.length-1-i].number));
    }

    for (let i = 0; i < player.numbers.length; i++){
      display.push(`${('0'+(Math.floor(Math.random()*63)+1)).slice(-2)}?`);
    }

    this.numericDisplay = display;
  }

  private displayEffect(){
    const amount = this.mp.getPlayerNumbers().length;
    
    if (amount > 0){
      const src = interval(2000/(amount**1.5)).subscribe(
        () => {
          src.unsubscribe();
          this.updateDisplay();
          this.displayEffect();
        }
      );  
    }
  }

}