import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { CommunicationService } from 'src/app/services/communication.service';
import { Router } from '@angular/router';
import { AudioService } from 'src/app/services/audio.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.scss']
})
export class JoinComponent implements OnInit {

  form_lobbyCreateTitle: string = '';
  form_err_lobbyCreateTitle: string = '';
  
  form_playerNick: string = '';
  form_err_playerNick: string = '';

  form_lobbyId: string = '';

  constructor(
    private mp: CommunicationService,
    private alert: AlertService,
    private route: Router,
    private audioService: AudioService
  ) { }

  ngOnInit(): void {
    this.audioService.loadAudio();
  }

  errorReset(which?: number){
    if (which){
      switch (which){
        case 1:
          this.form_err_lobbyCreateTitle = '';
          break;
        case 2:
          this.form_err_playerNick = '';
          break;
      }
    }
    else{
      this.form_err_lobbyCreateTitle = '';
      this.form_err_playerNick = '';
    }
  }

  modalOpen(primary?: boolean){
    this.errorReset();

    if (primary){
      if (this.form_lobbyId != ''){
        this.mp.exists(this.form_lobbyId).then(
          (exists) => {
            if (exists){
              this.mp.hasStarted(this.form_lobbyId).then(
                (started) => {
                  if (started){
                    this.alert.notify('error', 5000, 'Você não pode se juntar à um jogo em andamento!');
                  }
                  else{
                    const btn = <HTMLButtonElement>document.getElementById('joinLobbyModal-open');
                    btn.click();
                  }
                }
              );
            }
            else{
              this.alert.notify('error', 5000, 'A sala não foi encontrada.');
            }
          },
          () => { this.alert.notify('error', 5000, 'A sala não foi encontrada.'); }
        );
      }
      else{
        this.alert.notify('question', 5000, 'Você precisa digitar a identificação da sala que deseja entrar.');
      }
    }
    else{
      const btn = <HTMLButtonElement>document.getElementById('createLobbyModal-open');
      btn.click();
    }
  }

  lobbyCreate(){
    if (this.form_playerNick == ''){ this.form_err_playerNick = 'Você precisa de um apelido!'; }
    if (this.form_lobbyCreateTitle == ''){ this.form_err_lobbyCreateTitle = 'Sua sala precisa de um título!'; }

    if (!this.form_err_playerNick && !this.form_err_lobbyCreateTitle){
      this.mp.create(this.form_lobbyCreateTitle, this.form_playerNick).then(
        () => {
          this.mp.updatePlayer(this.form_playerNick);
          this.route.navigate(['/lobby']);
          this.alert.notify('success', 5000, 'Sala criada com sucesso!'); 
          //Dismiss Modal
          const btn = <HTMLButtonElement>document.getElementById('createLobbyModal-dismiss');
          btn.click();
        },
        () => { this.alert.notify('error', 5000, 'Não foi possível criar uma sala no momento, por favor tente novamente mais tarde.'); }
      );
    }
  }

  lobbyJoin(){
    if (this.form_playerNick == ''){ 
      this.form_err_playerNick = 'Você precisa de um apelido!'; 
    } 
    else{ 
      this.mp.isNickAvailable(this.form_lobbyId, this.form_playerNick).then(
        (available) => {
          if (available){
            this.mp.join(this.form_lobbyId, this.form_playerNick).then(
              () => {
                this.mp.updatePlayer(this.form_playerNick);
                this.route.navigate(['/lobby']);
                this.alert.notify('success', 5000, 'Você juntou-se à sala com sucesso.'); 
                //Dismiss Modal
                const btn = <HTMLButtonElement>document.getElementById('joinLobbyModal-dismiss');
                btn.click();
              },
              () => { this.alert.error('Falha ao juntar-se à sala!', 'Por favor tente novamente mais tarde.'); }
            ); 
          }
          else{
            this.form_err_playerNick = 'O apelido escolhido já está em uso nesta sala!'; 
          } 
        },
        () => { this.alert.error('Falha ao visualizar sala!', 'Por favor tente novamente mais tarde.'); }
      );
    }
  }

}
