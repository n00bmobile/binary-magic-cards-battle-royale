import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './components/game/game.component';
import { JoinComponent } from './components/join/join.component';
import { LobbyComponent } from './components/lobby/lobby.component';
import { RouteGuard } from './services/route-guard.service';

const routes: Routes = [

  //{path:'', redirectTo:'image/upload', pathMatch:'full'}, //path:'' = default route
  { path: '', component: JoinComponent },

  { path: 'lobby', component: LobbyComponent, canActivate: [RouteGuard] },
  { path: 'game', component: GameComponent, canActivate: [RouteGuard] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
