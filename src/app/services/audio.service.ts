import { Injectable } from '@angular/core';
import { LoadingService } from './loading.service';

type playableAudio = 'attack' | 'rightanswer' | 'wronganswer' | 'attacked01' | 'attacked02' | 'attacked03';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  public attackAudio = new Audio('../../../../assets/sounds/ingame/attack.wav');
  public rightAnswerAudio = new Audio('../../../assets/sounds/ingame/right_answer.wav');
  public wrongAnswerAudio = new Audio('../../../assets/sounds/ingame/wrong_answer.wav');
  public attacked01Audio = new Audio('../../../assets/sounds/ingame/attacked_01.wav');
  public attacked02Audio = new Audio('../../../assets/sounds/ingame/attacked_02.wav');
  public attacked03Audio = new Audio('../../../assets/sounds/ingame/attacked_03.wav');

  constructor(
    private loadingService: LoadingService
  ) { }

  playAudio(audio: playableAudio){
    switch (audio){
      case 'attack':
        this.attackAudio.pause();
        this.attackAudio.currentTime = 0;
        this.attackAudio.play();
        break;
      case 'rightanswer':
        this.rightAnswerAudio.pause();
        this.rightAnswerAudio.currentTime = 0;
        this.rightAnswerAudio.play();
        break;
      case 'wronganswer':
        this.wrongAnswerAudio.pause();
        this.wrongAnswerAudio.currentTime = 0;
        this.wrongAnswerAudio.play();
        break;
      case 'attacked01':
        this.attacked01Audio.pause();
        this.attacked01Audio.currentTime = 0;
        this.attacked01Audio.play();
        break;
      case 'attacked02':
        this.attacked02Audio.pause();
        this.attacked02Audio.currentTime = 0;
        this.attacked02Audio.play();
        break;
      case 'attacked03':
        this.attacked03Audio.pause();
        this.attacked03Audio.currentTime = 0;
        this.attacked03Audio.play();
        break;
    }
  }

  loadAudio(){
    let loadedCount = 0;
    this.loadingService.startLoading();
    //Load audios
    this.attackAudio.load();
    this.attackAudio.addEventListener('canplaythrough', 
      () => {
        loadedCount += 1;
        
        if (loadedCount == 6){
          this.loadingService.stopLoading();
        }
      }, 
    false);
    this.rightAnswerAudio.load();
    this.rightAnswerAudio.addEventListener('canplaythrough', 
      () => {
        loadedCount += 1;
        
        if (loadedCount == 6){
          this.loadingService.stopLoading();
        }
      }, 
    false);
    this.wrongAnswerAudio.load();
    this.wrongAnswerAudio.addEventListener('canplaythrough', 
      () => {
        loadedCount += 1;
        
        if (loadedCount == 6){
          this.loadingService.stopLoading();
        }
      }, 
    false);
    this.attacked01Audio.load();
    this.attacked01Audio.addEventListener('canplaythrough', 
      () => {
        loadedCount += 1;
        
        if (loadedCount == 6){
          this.loadingService.stopLoading();
        }
      }, 
    false);
    this.attacked02Audio.load();
    this.attacked02Audio.addEventListener('canplaythrough', 
      () => {
        loadedCount += 1;
        
        if (loadedCount == 6){
          this.loadingService.stopLoading();
        }
      }, 
    false);
    this.attacked03Audio.load();
    this.attacked03Audio.addEventListener('canplaythrough', 
      () => {
        loadedCount += 1;
        
        if (loadedCount == 6){
          this.loadingService.stopLoading();
        }
      }, 
    false);
  }

}