import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { CommunicationService } from './communication.service';

@Injectable({
  providedIn: 'root'
})
export class RouteGuard {

  constructor(
    public router: Router,
    public mp: CommunicationService
  ) { }
  
  /*canActivateChild(): boolean {
    return this.canActivate();
  }*/

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const destiny = route.url[0].path;

    if (destiny === ''){
      return true;
    }
    else if (this.mp.isInGame()){
      switch (this.mp.getState()){
        case 0:
          if (destiny === 'lobby'){ return true; }
          break;
        case 1:
          if (destiny === 'game'){ return true; }
          break;
      }
    }

    this.router.navigate(['']);
    return false;
  }

}
