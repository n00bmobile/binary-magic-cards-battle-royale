import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  public PAGE_LOADING: boolean = false;

  constructor() { }

  startLoading(){
    this.PAGE_LOADING = true;
  }

  stopLoading(){
    this.PAGE_LOADING = false;
  }

}
