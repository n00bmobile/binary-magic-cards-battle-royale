import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Router } from '@angular/router';
import { interval, Subject } from 'rxjs';
import { HiddenNumberModel } from '../models/hidden-number.model';
import { LobbyModel } from '../models/lobby.model';
import { PlayerModel } from '../models/player.model';
import { AlertService } from './alert.service';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  public CURRENT_GAME: LobbyModel = new LobbyModel();

  constructor(
    private db: AngularFireDatabase,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private route: Router
  ) { }

  private channel: any;
  private localPlayerNumbersSubject = new Subject<HiddenNumberModel[]>();

  private CURRENT_GAME_set(res: any){
    this.CURRENT_GAME.started_at = (<number>res[2] || 0);
    this.CURRENT_GAME.title = (<string>res[4] || '');
    this.CURRENT_GAME.state = (<number>res[3] || 0);
    this.CURRENT_GAME.owner = (new PlayerModel().set(res[0]));
    //Update players
    let players: PlayerModel[] = [];
    const data = <any>res[1];
    
    Object.keys(data).forEach(
      (key) => { 
        const ply = (new PlayerModel().set(data[key]));

        if (key == this.LocalPlayer().nickname){
          if (ply.numbers.length != this.LocalPlayer().numbers.length){
            this.localPlayer = ply;
            this.localPlayerNumbersSubject.next(ply.numbers);
          }
          else{
            this.localPlayer = ply;
          }
        }

        players.push(ply);
      }
    );
    this.CURRENT_GAME.players = players;
  }

  /*private CURRENT_GAME_update(){
    this.channel = this.db.list(`${this.CURRENT_GAME.id}`, ref => ref.orderByKey()).valueChanges().subscribe(
      (res) => { this.CURRENT_GAME_update_set(res); }
    );
  }*/

  private resetGame(){
    if (this.channel){
      this.channel.unsubscribe();
      this.channel = null;
    }
    
    this.CURRENT_GAME = new LobbyModel();
  }

  private loadGame(id: string){
    this.loadingService.startLoading();
    
    this.resetGame();
    this.CURRENT_GAME.id = id;

    const request = this.db.list(`${this.CURRENT_GAME.id}`, ref => ref.orderByKey()).valueChanges();
    this.channel = request.subscribe(
      (res) => { this.CURRENT_GAME_set(res); }
    );

    const promise = new Promise((resolve, reject) => {
      const obs = request.subscribe(
        () => {
          obs.unsubscribe();
          resolve(true);
        },
        (err) => {
          obs.unsubscribe();
          reject(err);
        }
      );
    });
    
    return promise;
  }

  private addPlayer(nick: string){
    let player = new PlayerModel();
    player.nickname = nick;
    player.last_seen = (new Date().getTime());
    return player;
  }

  watchLocalPlayerNumbers(){
    return this.localPlayerNumbersSubject;
  }

  create(title: string, nick: string){
    const owner = this.addPlayer(nick);
    const id = this.db.createPushId();
    
    const ref = this.db.list(id);
    ref.set('owner', owner);
    ref.set('title', title);
    ref.set('players', {});
    ref.set('state', 0);
    ref.set('started_at', 0);

    return this.join(id, nick);
  }

  join(id: string, nick: string){
    this.loadingService.startLoading();
    
    const promise = new Promise((resolve, reject) => {
      const ref = this.db.list(`${id}/players`);
      const player = this.addPlayer(nick);

      ref.set(nick, player).then(
        (res) => { 
          this.loadGame(id).then(
            () => {
              this.loadingService.stopLoading();
          
              this.localPlayer.nickname = player.nickname;
              this.localPlayer.numbers = [];
              this.localPlayer.numbers_guessed = [];
              this.localPlayer.last_seen = player.last_seen;

              this.timeOut();

              resolve(res); 
            },
            (err) => {
              this.loadingService.stopLoading();
              reject(err); 
            }
          );
        },
        (err) => { 
          this.loadingService.stopLoading();
          reject(err); 
        }
      );
    });

    return promise;
  }

  timeOut(){
    const src = interval(500).subscribe(
      () => {
        if (this.getState() < 1){ //In lobby
          if (this.isPlayerDisconnected(this.CURRENT_GAME.owner.nickname)){
            src.unsubscribe();

            this.resetGame();
            this.route.navigate(['']);
            this.alertService.notify('info', 10000, 'O dono da sala se desconectou.');

            return;
          }
          
          if (this.isOwner()){
            const players = this.getPlayers();

            for (let i = 0; i < players.length; i++){
              if (this.isPlayerDisconnected(players[i].nickname)){ 
                this.remove(players[i].nickname); /*
                  If the player's data start to update again he will reappear, regarless of game state. This code mainly prevents 
                  players who have closed the game's window and have no intent of playing anymore of still appearing in the lobby.
                */
              }
            }
          }
        }
        else if (this.isPlayerDefeated()){
          src.unsubscribe();
          return;
        }

        //Update only .last_seen (might put this in a function later)...
        const ref = this.db.list(`${this.CURRENT_GAME.id}/players/${this.LocalPlayer().nickname}`);
        ref.set('last_seen', (new Date().getTime())).catch(
          () => {
            src.unsubscribe();

            this.resetGame();
            this.route.navigate(['']);
            this.alertService.error('Falha ao se comunicar com a sala!', 'Atualização de vida falhou.');
          }
        );
      }
    );
  }

  remove(nick: string){
    const ref = this.db.list(`${this.CURRENT_GAME.id}/players`);
    return ref.remove(nick);
  }

  /** 
   * Updates all player data stored on Firebase's Realtime database. 
   * 
   * Warning: As this is an asynchronous operation updates that are yet to be completed might overlap each 
   * other (as they're not ordered like here, duh). THIS IS WHAT WAS BREAKING THE TIMEOUT FUNCTION!
   */
  update(nick: string, data: PlayerModel){ 
    const ref = this.db.list(`${this.CURRENT_GAME.id}/players`);
    return ref.set(nick, data);
  }

  startGame(){
    const ref = this.db.list(`${this.CURRENT_GAME.id}`); 
    ref.set('started_at', (new Date().getTime()));
    return ref.set('state', 1); //0 = in lobby; 1 = playing; 2 = ended
  }

  endGame(){
    const ref = this.db.list(`${this.CURRENT_GAME.id}`);
    return ref.set('state', 2); //0 = in lobby; 1 = playing; 2 = ended
  }

  leave(){
    this.resetGame();
  }

  exists(id: string){
    this.loadingService.startLoading();

    const promise = new Promise((resolve, reject) => {
      const request = this.db.list(`${id}/players`, ref => ref.orderByKey()).valueChanges().subscribe(
        (res) => {
          this.loadingService.stopLoading();

          request.unsubscribe();
          resolve((res.length > 0));
        },
        (err) => { 
          this.loadingService.stopLoading();
          
          request.unsubscribe();
          reject(err); 
        }
      );
    });
    
    return promise;
  }

  isNickAvailable(id: string, nick: string){
    this.loadingService.startLoading();
    
    const promise = new Promise((resolve, reject) => {
      const request = this.db.list(`${id}/players`, ref => ref.orderByKey().equalTo(nick)).valueChanges().subscribe(
        (res) => {
          this.loadingService.stopLoading();

          request.unsubscribe();
          resolve((res.length == 0));
        },
        (err) => { 
          this.loadingService.stopLoading();
          
          request.unsubscribe();
          reject(err); 
        }
      );
    });
    
    return promise;
  }

  hasStarted(id: string){
    this.loadingService.startLoading();
    
    const promise = new Promise((resolve, reject) => {
      const request = this.db.list(`${id}`, ref => ref.orderByKey()).valueChanges().subscribe(
        (res) => {
          this.loadingService.stopLoading();

          const state = <number>res[3];
          request.unsubscribe();
          resolve((state == 1));
        },
        (err) => {
          this.loadingService.stopLoading();

          request.unsubscribe();
          reject(err);
        }
      );
    });
    
    return promise;
  }

  onStarted(){
    const promise = new Promise((resolve, reject) => {
      const request = this.db.list(`${this.CURRENT_GAME.id}`, ref => ref.orderByKey()).valueChanges().subscribe(
        (res) => {
          const state = <number>res[3];

          if (state >= 1){
            request.unsubscribe();
            resolve(true);
          }
        },
        (err) => {
          request.unsubscribe();
          reject(err);
        }
      );
    });

    return promise;
  }

  //LOCAL PLAYER
  localPlayer: PlayerModel = new PlayerModel();

  updatePlayer(data: PlayerModel | string){
    if (typeof data == 'string'){
      this.localPlayer.nickname = data;
      return this.update(data, this.localPlayer);
    }
    else{
      if (data.numbers.length != this.LocalPlayer().numbers.length){
        this.localPlayerNumbersSubject.next(data.numbers);
        this.localPlayer = data;
        return this.update(data.nickname, data);
      }
      else{
        this.localPlayer = data;
        return this.update(data.nickname, data);
      }
    }
  }

  //GETTERS FOR LAZY PEOPLE...
  getWinner(){
    if (this.isInGame()){
      const playersLeft =  this.getPlayers().filter(ply => { return !this.isPlayerOut(ply.nickname); });

      if (playersLeft.length <= 1){
        return this.getPlayerScoreboard()[0];
      }
    }

    return null;
  }

  isInGame(){
    if (this.CURRENT_GAME.id){
      return true;
    }

    return false;
  }

  isNearLosing(nick?: string){
    return (!this.isPlayerOut(nick || this.LocalPlayer().nickname) && this.getPlayerNumbers(nick).length > 7);
  }

  getPlayer(nick?: string){
    return this.db.list(`${this.CURRENT_GAME.id}/players/${nick || this.LocalPlayer().nickname}`);
  }

  getPlayerLocally(nick?: string): PlayerModel{
    if (nick){
      return <PlayerModel>this.getPlayers().find(element => { return element.nickname == nick; });
    }
    else{
      return this.LocalPlayer();
    }
  }

  LocalPlayer(){
    return this.localPlayer;
  }

  isPlayerDefeated(nick?: string){
    return (this.getPlayerNumbers(nick).length > 8);
  }

  isPlayerDisconnected(nick?: string){
    const ply = this.getPlayerLocally(nick);
    
    if (ply){
      const present = (new Date().getTime());
      const past = this.getPlayerLocally(nick).last_seen;
      return (present-past > 8000); //6 tries...
    }
    else{
      return true;
    }
  }

  isPlayerOut(nick?: string){ //Check if a player either lost or has disconnected...
    return (this.isPlayerDefeated(nick) || this.isPlayerDisconnected(nick));
  }

  getPlayerScoreboard(){
    const players = this.getPlayers();
    let dead: PlayerModel[] = [];
    let alive = players.filter(
      (ply) => { 
        if (this.isPlayerOut(ply.nickname)){
          dead.push(ply);
          return false;
        }
        else{
          return true;
        }
      }
    ).sort(function(ply1, ply2) {
      /*const ng1 = ply1.numbers_guessed;
      const ng2 = ply2.numbers_guessed;

      if (ng1.length == ng2.length){
        return ply1.numbers.length - ply2.numbers.length;
      }
      else{
        return ng2.length - ng1.length;
      }*/

      return ply1.numbers.length - ply2.numbers.length;
    });
    dead = dead.sort(function(ply1, ply2) { return (ply2.last_seen - ply1.last_seen); });

    return alive.concat(dead); 
  }

  getPlayerPositionOnScoreboard(){
    const players = this.getPlayerScoreboard();
    
    for (let i = 0; i < players.length; i++){
      if (players[i].nickname == this.LocalPlayer().nickname){
        return i;
      }
    }

    return 0;
  }

  getUpdates(){
    return this.db.list(`${this.CURRENT_GAME.id}/players`, ref => ref.orderByKey()).valueChanges();
  }

  isOwner(){
    return (this.CURRENT_GAME.owner?.nickname == this.localPlayer.nickname);
  }

  getPlayers(){
    return (this.CURRENT_GAME.players || []);
  }

  getPlayersLeft(){
    const players = this.getPlayers();
    let playersAlive: PlayerModel[] = [];
    
    for (let i = 0; i < players.length; i++){
      if (!this.isPlayerOut(players[i].nickname)){
        playersAlive.push(players[i]);
      }
    }

    return playersAlive;
  }

  getMatchTime(){
    const present = new Date().getTime();
    const past = this.getStartedAt();

    if (past > 0){
      return (present-past);
    }

    return 0;
  }

  getStartedAt(){
    return (this.CURRENT_GAME.started_at || 0);
  }

  getState(){
    return (this.CURRENT_GAME.state || 0);
  }

  getTitle(){
    return (this.CURRENT_GAME.title || '');
  }

  getId(){
    return (this.CURRENT_GAME.id || '');
  }

  getPlayerNumbers(nick?: string){
    const players = this.getPlayers();
    
    if (nick){
      for (let i = 0; i < players.length; i++){
        if (players[i].nickname == nick){
          return players[i].numbers;
        }
      }
    }
    else{
      return this.LocalPlayer().numbers;
    }

    return [];
  }

}