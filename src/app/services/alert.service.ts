import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon, SweetAlertResult, SweetAlertInput } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  async stringRequest(input: SweetAlertInput, label: string, placeholder?: string){
    const { value: str } = await Swal.fire({
      input: input,
      inputLabel: label,
      inputPlaceholder: (placeholder || '')
    })
    
    if (str) {
      return str;
    }
  }

  notify(icon: SweetAlertIcon, duration: number, message: string){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: duration,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    
    Toast.fire({
      icon: icon,
      title: message
    })
  }

  custom(title: string, text: string, img: string, width: number, height: number, alt?: string){
    Swal.fire({
      title: title,
      text: text,
      imageUrl: img,
      imageWidth: width,
      imageHeight: height,
      imageAlt: (alt || 'Imagem da notificação'),
      confirmButtonText: "Ok",
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-primary',
      }
    });
  }

  alert(title: string, text: string){
    Swal.fire({
      title: title,
      text: text,        
      icon: "success",
      confirmButtonText: "Ok",
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-primary',
      }
    });
  }

  error(title: string, text: string){
    Swal.fire({
      title: title,
      text: text,
      icon: "error",
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-warning',
      }
    });
  }

  confirm(title: string, text: string): Promise<SweetAlertResult>{
    return Swal.fire({
      title: title,
      text: text,
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Confirmar",
      cancelButtonText: "Cancelar",
      buttonsStyling: false,
      customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-danger",    
      }
    });
  }

}
