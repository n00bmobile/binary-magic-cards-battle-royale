import { HiddenNumberModel } from "./hidden-number.model";

export class PlayerModel{

  nickname: string = '';

  numbers: HiddenNumberModel[] = [];
  numbers_guessed: HiddenNumberModel[] = [];
  ammo: number[] = [];

  last_seen: number = 0;

  set(data: PlayerModel){
    let model = new PlayerModel();

    model.nickname = (data.nickname || '');
    
    model.numbers = (data.numbers || []);
    model.numbers_guessed = (data.numbers_guessed || []);
    model.ammo = (data.ammo || []);

    model.last_seen = (data.last_seen || 0);
   
    return model;
  }
  
}