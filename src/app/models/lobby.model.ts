import { PlayerModel } from "./player.model";

export class LobbyModel{
  
  id?: string;

  title!: string;
  owner!: PlayerModel;
  state!: number;

  players!: PlayerModel[];
  
  started_at!: number;
  
}